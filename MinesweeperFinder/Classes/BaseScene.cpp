#include "BaseScene.h"
#include "SelectScene .h"

bool BaseScene::init()
{
	if (!Super::init())
		return false;

	_mainNode.reset(CSLoader::createNode("Game.csb"));
	if (_mainNode.get() && initGame())
	{
		this->addChild(_mainNode.get());
		return true;
	}
	return false;
}

bool BaseScene::initGame()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	auto pnlCont = _mainNode->getChildByName("pnlContainer");
	if (pnlCont)
	{
		auto pnlSize = pnlCont->getContentSize();
		pnlCont->setScaleX(visibleSize.width / pnlSize.width);
		pnlCont->setScaleY(visibleSize.height / pnlSize.height);

		auto subPnl = pnlCont->getChildByName("pnlContainer");
		_pnlFileld = subPnl->getChildByName("pnlField");
		if (_pnlFileld)
		{
			_cellTmp = (Cell*)_pnlFileld->getChildByName("btn_tmp");
			_pnlFileld->removeChildByName("btn_tmp");
			_txtResult = (ui::Text*)_pnlFileld->getChildByName("txtResult");
			_txtResult->setLocalZOrder(10);

			_btnBack = (ui::Button*)subPnl->getChildByName("btnBack");
			if (_btnBack)
				_btnBack->addClickEventListener(CC_CALLBACK_1(BaseScene::onBack, this));

			initField();
			return true;
		}
	}
	return false;
}

void BaseScene::onBack(Ref * pSender)
{
	auto scene = SelectScene::create();
	if (scene)
		Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene, Color3B(255, 204, 0)));
}

vector<int> BaseScene::getDigits(string data) const
{
	vector<int> ji;
	if (data.length() == 1)
		data = "0" + data;

	for (int j = 0; j < 2; j++)
		ji.push_back(int(data[j] - '0'));
	return ji;
}

int BaseScene::randomInt(int min, int max) const
{
	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> uni(min, max);
	return uni(rng);
}

void BaseScene::onEnter()
{
	Super::onEnter();
	scheduleUpdate();
}

void BaseScene::onExit()
{
	Super::onExit();
	unscheduleUpdate();
}

