#include "AppDelegate.h"
#include "SelectScene .h"
USING_NS_CC;

bool AppDelegate::applicationDidFinishLaunching()
{
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if(!glview)
	{
		glview = GLViewImpl::create( "Game" );
		director->setOpenGLView( glview );
	}

	director->setDisplayStats( false );
	director->setAnimationInterval( 1.0 / 60.0f );
	auto scene = SelectScene::create();
	director->runWithScene( scene );

	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
	Director::getInstance()->stopAnimation();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
	Director::getInstance()->startAnimation();
}
