#ifndef __FINDER_SCENE_H__
#define __FINDER_SCENE_H__

#include "preheader.h"
#include "BaseScene.h"

class FinderScene : public BaseScene
{
	enum class Move
	{
		None = -1,
		UP = (int)EventKeyboard::KeyCode::KEY_UP_ARROW,
		DOWN = (int)EventKeyboard::KeyCode::KEY_DOWN_ARROW,
		LEFT = (int)EventKeyboard::KeyCode::KEY_LEFT_ARROW,
		RIGHT = (int)EventKeyboard::KeyCode::KEY_RIGHT_ARROW
	};

public:
	CREATE_FUNC(FinderScene);
	void onEnter();
	void onExit();

private:
	struct CellData
	{
		string value;
		vector<int> pos;
	};

	void initField();
	Cell* initFreeCell(Cell* cell, const string& value) const;
	Cell* getFreeRandomCell() const;
	bool isFreeCell(Cell* cell) const;
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void move(int keyCode);
	bool isValid(const int& i) const;
	void checkGameState();
	vector<int> getPath(const string& str);
	bool isWin(vector<int>& vec);
	bool isLose(vector<int>& vec);
	void onCell(Ref* pSender);
	void disableControls();

	const static int n = 10;
	const string obsc = "#";
	const string start = "S";
	const string finish = "F";
	const string UP = "U";
	const string DOWN = "D";
	const string LEFT = "L";
	const string RIGHT = "R";

	Cell* _cells[n][n];
	vector<int> _sCell;
	vector<int> _fCell;
};
#endif
