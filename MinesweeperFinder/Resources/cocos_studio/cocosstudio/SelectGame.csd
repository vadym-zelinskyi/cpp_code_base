<GameFile>
  <PropertyGroup Name="SelectGame" Type="Node" ID="525ae787-a344-46f7-8425-182cf3e3279d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="45" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="pnlContainer" ActionTag="1513077061" Tag="13" IconVisible="False" RightMargin="-1280.0000" TopMargin="-800.0000" ClipAble="False" BackColorAlpha="144" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="800.0000" />
            <Children>
              <AbstractNodeData Name="pnlContainer" ActionTag="314981271" Tag="47" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" ClipAble="False" BackColorAlpha="43" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="1280.0000" Y="800.0000" />
                <Children>
                  <AbstractNodeData Name="pnlField" ActionTag="1681713147" Tag="34" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="240.0000" RightMargin="240.0000" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="800.0000" Y="800.0000" />
                    <Children>
                      <AbstractNodeData Name="btnFinder" ActionTag="-1628169338" UserData="1" Tag="35" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="200.0000" RightMargin="200.0000" TopMargin="347.0400" BottomMargin="372.9600" TouchEnable="True" FontSize="55" ButtonText="Finder" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="38" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                        <Size X="400.0000" Y="80.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="400.0000" Y="412.9600" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5162" />
                        <PreSize X="0.5000" Y="0.1000" />
                        <FontResource Type="Normal" Path="fonts/Marker Felt.ttf" Plist="" />
                        <TextColor A="255" R="0" G="128" B="0" />
                        <DisabledFileData Type="Normal" Path="Res/btn_dis.png" Plist="" />
                        <PressedFileData Type="Normal" Path="Res/btn_dis.png" Plist="" />
                        <NormalFileData Type="Normal" Path="Res/btn_press.png" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="btnMines" ActionTag="-1851765431" UserData="1" Tag="17" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="200.0000" RightMargin="200.0000" TopMargin="481.2800" BottomMargin="238.7200" TouchEnable="True" FontSize="55" ButtonText="Minesweeper" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="38" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                        <Size X="400.0000" Y="80.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="400.0000" Y="278.7200" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.3484" />
                        <PreSize X="0.5000" Y="0.1000" />
                        <FontResource Type="Normal" Path="fonts/Marker Felt.ttf" Plist="" />
                        <TextColor A="255" R="0" G="128" B="0" />
                        <DisabledFileData Type="Normal" Path="Res/btn_dis.png" Plist="" />
                        <PressedFileData Type="Normal" Path="Res/btn_dis.png" Plist="" />
                        <NormalFileData Type="Normal" Path="Res/btn_press.png" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="txtSelect" ActionTag="-1252024158" Tag="18" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="217.0000" RightMargin="217.0000" TopMargin="134.0000" BottomMargin="582.0000" FontSize="80" LabelText="Select Game" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="366.0000" Y="84.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="400.0000" Y="624.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.7800" />
                        <PreSize X="0.4575" Y="0.1050" />
                        <FontResource Type="Normal" Path="fonts/Marker Felt.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="640.0000" Y="400.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.6250" Y="1.0000" />
                    <SingleColor A="255" R="127" G="127" B="127" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="191" G="191" B="191" />
                <FirstColor A="255" R="255" G="0" B="189" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>