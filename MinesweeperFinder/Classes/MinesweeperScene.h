#ifndef __MINES_SCENE_H__
#define __MINES_SCENE_H__

#include "preheader.h"
#include "BaseScene.h"

class MinesweeperScene : public BaseScene
{
public:
	CREATE_FUNC(MinesweeperScene);

private:
	struct CellData
	{
		int value;
		int i;
		int j;
	};

	enum GameState
	{
		Win = 1,
		Lose = 10
	};
	
	const static int n = 10;
	const int BOMBS = 10;

	void onCell(Ref* pSender);
	void initField();
	void refreshField(Cell* button);
	void openCells(int i, int j);
	Color3B getColor(int value) const;
	void updateGameState(GameState state);

	int _field[n][n];
	Cell* _cells[n][n];
	int _cellsLeft;
};
#endif
