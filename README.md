#It's just a simple C++ code base projects.

1. PalindromeString - checks if some line of text is palindrome

2. BracketsChecker - checks if open brackets comply to closed brackets

3. MinesweeperFinder - two games Minesweeper and Finder just in one project.
It was made with Cocos2dx framework (C++ API)
a) Minesweeper example - http://minesweeperonline.com/;
b) Finder - just find the most short way from one given location(S) to an another(F), but on each step a new obstacle appears on a random place.