// BracketsChecker.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <regex>
#include <map>
#include <sstream>
#include <iostream>
using namespace std;

/*template<typename T, typename K>
int getComply(T s, K p)
{
	int complyOpen = 0;
	for (int i = 0; i < s.length(); i++)
		if (s[i] == p)
			complyOpen++;
	return complyOpen;
}*/

int isBracketsDone(const string& str)
{
	char open[] = "[{(";
	char close[] = "]})";
	int comply = 0;

	//For getComply(...) we can use lambda or template function
	auto getComply = [](const string& s, char p)-> int{
		int complyOpen = 0;
		for (int i = 0; i < s.length(); i++)
			if (s[i] == p)
				complyOpen++;
		return complyOpen;
	};

	for (int i = 0; i < strlen(open); i++)
	{
		int openC = getComply(str, open[i]);
		int closeC = getComply(str, close[i]);
		if (openC == closeC)
		{
			if (openC > 0 && closeC > 0)
				comply++;
		}
		else
		{
			return -1;
		}
	}
	return comply;
}


int _tmain(int argc, _TCHAR* argv[])
{
	string str;
	cout << "Please enter an input string: ";
	getline(cin, str);

	int comply = isBracketsDone(str);
	if (comply == 0)
		cout << "WRN: input string has no brackets!\n";
	else if (comply > 0)
		cout << str << ":" << " open brackets are comply to closed brackets\n";
	else
		cout << str << ":" << " open brackets are not comply to closed brackets\n";
	system("pause");
	return 0;
}

