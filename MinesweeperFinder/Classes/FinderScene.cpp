#include "FinderScene.h"
USING_NS_CC;

void FinderScene::initField()
{
	int xPos = 0;
	int yPos = _pnlFileld->getContentSize().height;
	Size btnSize = _cellTmp->getContentSize();
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			Cell* cell = (Cell*)_cellTmp->clone();
			_cells[i][j] = cell;
			CellData* data = new CellData();
			cell->setUserData(reinterpret_cast<void*>(data));
			data->pos = {i, j};
			data->value = "";
			cell->setPosition(Vec2(xPos, yPos));
			cell->addClickEventListener(CC_CALLBACK_1(FinderScene::onCell, this));
			_pnlFileld->addChild(cell);
			xPos += btnSize.height;
		}
		xPos = 0;
		yPos -= btnSize.height;
	}

	// Set Obstacles positions
	int obstacles = randomInt(0, 10);
	for (int i = 0; i < obstacles; i++)
		initFreeCell(getFreeRandomCell(), obsc);

	// Set Start and Finish positions
	Cell* sc = initFreeCell(getFreeRandomCell(), start);
	if (sc)
		_sCell = (reinterpret_cast<CellData*>(sc->getUserData()))->pos;

	Cell* fc = initFreeCell(getFreeRandomCell(), finish);
	if(fc)
		_fCell = (reinterpret_cast<CellData*>(fc->getUserData()))->pos;

	checkGameState();
}

void FinderScene::checkGameState()
{
	auto up = getPath(UP);
	auto down = getPath(DOWN);
	auto left = getPath(LEFT);
	auto right = getPath(RIGHT);

	if (isWin(up) || isWin(down) || isWin(left) || isWin(right))
	{
		_txtResult->setVisible(true);
		_txtResult->setString("You WIN!");
		disableControls();
		return;
	}

	if (isLose(up) && isLose(down) && isLose(left) && isLose(right))
	{
		_txtResult->setVisible(true);
		_txtResult->setString("You LOSE!");
		disableControls();
	}
}

bool FinderScene::isWin(vector<int>& vec)
{
	return (isValid(vec[0]) && isValid(vec[1]) && vec == _fCell);
}

bool FinderScene::isLose(vector<int>& vec)
{
	if (isValid(vec[0]) && isValid(vec[1]))
		return !isFreeCell(_cells[vec[0]][vec[1]]);
	return true;
}

void FinderScene::onCell(Ref * pSender)
{
	auto cell = dynamic_cast<Cell*>(pSender);
	if (cell)
	{
		CellData* data = reinterpret_cast<CellData*>(cell->getUserData());
		if (data)
		{
			Move dir = Move::None;
			if (data->pos == getPath(UP))
				dir = Move::UP;
			if (data->pos == getPath(DOWN))
				dir = Move::DOWN;
			if (data->pos == getPath(LEFT))
				dir = Move::LEFT;
			if (data->pos == getPath(RIGHT))
				dir = Move::RIGHT;
			if (dir != Move::None)
				move(int(dir));
		}
	}
}

bool FinderScene::isFreeCell(Cell* cell) const
{
	return (reinterpret_cast<CellData*>(cell->getUserData())->value == "");
}

Cell* FinderScene::initFreeCell(Cell* cell, const string& value) const
{
	CellData* data = reinterpret_cast<CellData*>(cell->getUserData());
	data->value = value;

	cell->setEnabled(false);
	cell->setTitleText(value);
	cell->setTitleColor(Color3B::BLACK);
	return cell;
}

Cell* FinderScene::getFreeRandomCell() const
{
	vector<int> ji;
	Cell* cell;
	do
	{
		ji = getDigits(to_string(randomInt(0, 99)));
		cell = _cells[ji[0]][ji[1]];
	} while (!isFreeCell(cell));
	return cell;
}

bool FinderScene::isValid(const int& i) const
{
	return (i >= 0 && i < n);
}

void FinderScene::move(int dir)
{
	string path = "";
	switch (dir)
	{
	case (int)Move::UP: path = UP; break;
	case (int)Move::DOWN: path = DOWN; break;
	case (int)Move::LEFT: path = LEFT; break;
	case (int)Move::RIGHT: path = RIGHT; break;
	default: return;
	}

	auto pos = getPath(path);
	int i = pos[0], j = pos[1];
	if (isValid(i) && isValid(j) && isFreeCell(_cells[i][j]))
	{
		initFreeCell(_cells[i][j], path);
		initFreeCell(getFreeRandomCell(), obsc);
		_sCell[0] = i;
		_sCell[1] = j;
		checkGameState();
	}
}

vector<int> FinderScene::getPath(const string& value)
{
	vector<int> path(2, -1);
	if (value == UP)
	{
		path[0] = _sCell[0] - 1;
		path[1] = _sCell[1];
	}

	if (value == DOWN)
	{
		path[0] = _sCell[0] + 1;
		path[1] = _sCell[1];
	}

	if (value == LEFT)
	{
		path[0] = _sCell[0];
		path[1] = _sCell[1] - 1;
	}

	if (value == RIGHT)
	{
		path[0] = _sCell[0];
		path[1] = _sCell[1] + 1;
	}

	return path;
}

void FinderScene::onEnter()
{
	Super::onEnter();
	if (_keyEventListener == NULL)
	{
		_keyEventListener = EventListenerKeyboard::create();
		_keyEventListener->onKeyReleased = CC_CALLBACK_2(FinderScene::onKeyReleased, this);
	}

	_eventDispatcher->addEventListenerWithSceneGraphPriority(_keyEventListener, this);
	scheduleUpdate();
}

void FinderScene::onExit()
{
	Super::onExit();
	disableControls();
}

void FinderScene::disableControls()
{
	_eventDispatcher->removeEventListener(_keyEventListener);
	unscheduleUpdate();
}

void FinderScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	move(int(keyCode));
	log("Key with keycode %d released", keyCode);
}
