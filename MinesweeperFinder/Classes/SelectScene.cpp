#include "SelectScene .h"
#include "MinesweeperScene.h"
#include "FinderScene.h"


bool SelectScene::init()
{
	if ( !Super::init() )
		return false;

	_mainNode.reset(CSLoader::createNode("SelectGame.csb"));
	if (_mainNode.get() && initGame())
	{
		this->addChild(_mainNode.get());
		return true;
	}
	return false;
}

bool SelectScene::initGame()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	auto pnlCont = _mainNode->getChildByName("pnlContainer");
	if (pnlCont)
	{
		auto pnlSize = pnlCont->getContentSize();
		pnlCont->setScaleX(visibleSize.width / pnlSize.width);
		pnlCont->setScaleY(visibleSize.height / pnlSize.height);

		Node* pnlFileld = (pnlCont->getChildByName("pnlContainer"))->getChildByName("pnlField");
		if (pnlFileld)
		{
			_btnFinder = (ui::Button*)pnlFileld->getChildByName("btnFinder");
			_btnMines = (ui::Button*)pnlFileld->getChildByName("btnMines");
			if(_btnFinder)
				_btnFinder->addClickEventListener(CC_CALLBACK_1(SelectScene::onBtnFinder, this));
			if (_btnMines)
				_btnMines->addClickEventListener(CC_CALLBACK_1(SelectScene::onBtnMines, this));
			return true;
		}
	}
	return false;
}

void SelectScene::onBtnFinder(Ref* pSender)
{
	auto scene = FinderScene::create();
	if(scene)
		Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene, Color3B(255, 204, 0)));
}

void SelectScene::onBtnMines(Ref* pSender)
{
	auto scene = MinesweeperScene::create();
	if (scene)
		Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene, Color3B(255, 204, 0)));
}