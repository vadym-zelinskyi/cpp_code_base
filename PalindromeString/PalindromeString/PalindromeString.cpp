// PalindromeString.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <regex>
#include <sstream>
#include <iostream>
using namespace std;

bool isPalindrom(string str)
{
	regex remove("[,.:'/! ]");
	string rep = regex_replace(str, remove, "");
	auto charS = const_cast<char*>(rep.c_str());

	bool isPol = true;
	int len = strlen(charS);
	for (int i = 0, j = len - 1; i <= len/2; i++, j--)
	{
		if (tolower(charS[i]) != tolower(charS[j]))
		{
			isPol = false;
			break;
		}
	}
	return isPol;
}

int _tmain(int argc, _TCHAR* argv[])
{
	string str;
	cout << "Please enter some string: ";
	getline(cin, str);

	string val = "palindrome\n";
	if (!isPalindrom(str))
		val = "not " + val;
	cout << "\/" << str << "\/" << " is " << val;

	system("pause");
	return EXIT_SUCCESS;
}

