#ifndef __SELECT_SCENE_H__
#define __SELECT_SCENE_H__

#include "preheader.h"
#include "ui/UIButton.h"

class SelectScene : public cocos2d::Scene
{
	typedef cocos2d::Scene Super;
public:
	CREATE_FUNC(SelectScene);
	virtual bool init();
	bool initGame();

private:
	void onBtnMines(Ref* pSender);
	void onBtnFinder(Ref* pSender);

	shared_ptr<Node> _mainNode;
	ui::Button* _btnFinder;
	ui::Button* _btnMines;
};
#endif
