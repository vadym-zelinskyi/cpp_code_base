#include "MinesweeperScene.h"
USING_NS_CC;

void MinesweeperScene::initField()
{
	const int bomb = 9;
	auto valid = [this](int i)-> bool {
		return (i >= 0 && i < n);
	};

	auto dist = [this, &valid, &bomb](int i, int j) ->int {
		if (valid(i) && valid(j))
			if (_field[i][j] == bomb)
				return 1;
		return 0;
	};

	for (int i = 0; i < BOMBS; i++)
	{
		vector<int> ji = getDigits(to_string(randomInt(0, 99)));
		if (_field[ji[0]][ji[1]] != bomb)
			_field[ji[0]][ji[1]] = bomb;
		else
			i--;
	}

	int xPos = 0;
	int yPos = _pnlFileld->getContentSize().height;
	float btnSize = yPos / n;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (_field[i][j] != bomb)
				_field[i][j] = dist(i, j - 1) + dist(i, j + 1) + dist(i - 1, j) + dist(i + 1, j) + dist(i + 1, j + 1) + dist(i + 1, j - 1) + dist(i - 1, j - 1) + dist(i - 1, j + 1);

			Cell* cell = (Cell*)_cellTmp->clone();
			CellData* data = new CellData();
			data->i = i;
			data->j = j;
			data->value = _field[i][j];
			cell->setUserData(reinterpret_cast<void*>(data));

			_cells[i][j] = cell;
			cell->setPosition(Vec2(xPos, yPos));
			cell->addClickEventListener(CC_CALLBACK_1(MinesweeperScene::onCell, this));
			_pnlFileld->addChild(cell);
			xPos += btnSize;
			_cellsLeft++;
		}
		xPos = 0;
		yPos -= btnSize;
	}
}

void MinesweeperScene::onCell(Ref * pSender)
{
	auto cell = dynamic_cast<Cell*>(pSender);
	if (cell)
		refreshField(cell);
}

void MinesweeperScene::refreshField(Cell* cell)
{
	CellData* useData = reinterpret_cast<CellData*>(cell->getUserData());
	string btnTxt = std::to_string(useData->value);
	if (cell->isEnabled())
	{
		cell->setEnabled(false);
		_cellsLeft--;
		auto color = getColor(useData->value);
		switch (useData->value)
		{
		case 0:
			btnTxt = "";
			openCells(useData->i, useData->j);
			break;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			cell->setTitleColor(color);;
			break;
		case 9:
			updateGameState(GameState::Lose);
			return;
		default:
			break;
		}

		cell->setTitleText(btnTxt);
		if (_cellsLeft == BOMBS)
			updateGameState(GameState::Win);
	}
}

void MinesweeperScene::updateGameState(GameState state)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			auto button = _cells[i][j];
			if (button)
			{
				CellData* useData = reinterpret_cast<CellData*>(button->getUserData());
				string btnTxt = std::to_string(useData->value);
				if (btnTxt == "9")
				{
					button->setTitleText(btnTxt);
					button->setTitleColor(getColor(useData->value));
					if (state == GameState::Lose)
						button->setEnabled(false);
				}
				button->addClickEventListener(nullptr);
			}
		}
	}

	_txtResult->setVisible(true);
	_txtResult->setString( state == GameState::Lose ? "You LOSE!" : "You WIN!");
}

void MinesweeperScene::openCells(int i, int j)
{
	auto valid = [this](int i)-> bool {
		return (i >= 0 && i < n);
	};

	auto openCell = [this, &valid](int ni, int nj) {
		if (valid(ni) && valid(nj))
			refreshField(_cells[ni][nj]);
	};

	openCell(i, j - 1);//TO DO
	openCell(i, j + 1);
	openCell(i - 1, j);
	openCell(i + 1, j);
	openCell(i + 1, j + 1);
	openCell(i + 1, j - 1);
	openCell(i - 1, j - 1);
	openCell(i - 1, j + 1);
}

Color3B MinesweeperScene::getColor(int value) const
{
	switch (value)
	{
	case 1: return Color3B::BLUE;
	case 2: return Color3B(0, 153, 51);
	case 3: return Color3B::RED;
	case 4: return Color3B::ORANGE;
	case 5: return Color3B::MAGENTA;
	default: return Color3B::BLACK;
	}
}
