#ifndef __BASE_SCENE_H__
#define __BASE_SCENE_H__

#include "preheader.h"

class BaseScene : public cocos2d::Scene
{
public:
	typedef cocos2d::Scene Super;

	CREATE_FUNC(BaseScene);
	virtual bool init();
	virtual void onEnter();
	virtual void onExit();

	void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event) {}
	void onMouseDown(Event *event) {}
protected:
	virtual bool initGame();
	virtual void initField() {}
	void onBack(Ref* pSender);
	int randomInt(int min, int max) const;
	vector<int> getDigits(string data) const;

	shared_ptr<Node> _mainNode;
	Cell* _cellTmp;
	Cell* _btnBack;
	ui::Text * _txtResult;
	Node* _pnlFileld;

	EventListenerKeyboard* _keyEventListener = NULL;
	EventListenerMouse* _mouseEventListener = NULL;
};

#endif
